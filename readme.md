Quick start!
============

Install Meteor:

```
$ curl https://install.meteor.com | /bin/sh
```

Create a project:

```
$ meteor create stageAllocation
```

Run it locally:

```
$ cd stageAllocation
$ meteor
=> Meteor server running on: http://localhost:3000/i
```
